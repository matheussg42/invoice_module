<?php

namespace Tests\Unit;

use App;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\Modules\Invoices\Domain\Services\InvoiceService;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Domain\Exceptions\InvoiceNotFoundException;

class InvoiceTest extends TestCase
{
    private $invoiceRepository;
    private $approvalFacade;
    private $invoiceService;

    public function setUp(): void
    {
        parent::setUp();

        $this->invoiceRepository = new InvoiceRepository();
        $this->approvalFacade = $this->app->make('App\Modules\Approval\Application\ApprovalFacade');
        $this->invoiceService = new InvoiceService($this->invoiceRepository, $this->approvalFacade);
    }

    public function testInstanceOfInvoiceService()
    {
        $this->assertInstanceOf(InvoiceService::class, $this->invoiceService);
    }

    public function testShowReturnsInvoice()
    {
        $invoiceMock = DB::table('invoices')->select('*')->first();
        $invoice = $this->invoiceService->getInvoice($invoiceMock->id);

        $this->assertEquals($invoice->getId(), $invoiceMock->id);
    }

    public function testShowThrowsExceptionWhenInvoiceNotFound()
    {
        $uuid = '123';
        $this->expectException(InvoiceNotFoundException::class);
        $this->invoiceService->getInvoice($uuid);
    }

    public function testApproveInvoice()
    {
        $invoiceMock = DB::table('invoices')->select('*')->where('status', 'draft')->first();
        $invoice = $this->invoiceService->approveInvoice($invoiceMock->id);
        
        $this->assertEquals($invoice->getId(), $invoiceMock->id);
        $this->assertEquals($invoice->getStatus()->value, "approved");
    }

    public function testRejectInvoice()
    {
        $invoiceMock = DB::table('invoices')->select('*')->where('status', 'draft')->first();
        $invoice = $this->invoiceService->rejectInvoice($invoiceMock->id);
        
        $this->assertEquals($invoice->getId(), $invoiceMock->id);
        $this->assertEquals($invoice->getStatus()->value, "rejected");
    }
}