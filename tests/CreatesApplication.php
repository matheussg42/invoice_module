<?php

namespace Tests;

use Artisan;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    public function setUp():void{
        parent::setUp();
        $this->prepareForTests();
    }
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    public function prepareForTests(){
        $this->artisan('migrate:fresh');
        $this->artisan('db:seed', ['--class' => "App\\Modules\\Invoices\\Infrastructure\\Database\\Seeders\\DatabaseSeeder"]);
    }

    public function tearDown():void{
        parent::tearDown();
    }
}
