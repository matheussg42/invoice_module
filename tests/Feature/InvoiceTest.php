<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use App\Modules\Invoices\Domain\Entities\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Modules\Invoices\Domain\Services\InvoiceService;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;

class InvoiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private $invoiceRepository;
    private $invoiceService;

    public function setUp(): void
    {
        parent::setUp();
        $this->invoiceRepository = $this->app->make(InvoiceRepository::class);
        $this->invoiceService = $this->app->make(InvoiceService::class);
    }

    public function testShowInvoiceEndpoint()
    {

        $invoice = DB::table('invoices')
            ->select('*')
            ->first();

        $company = DB::table('companies')
            ->select('*')
            ->where('id', $invoice->company_id)
            ->first();

        $items[] = DB::table('invoice_product_lines')
            ->select(DB::raw('products.id, products.name, products.price, products.currency, quantity, (products.price * quantity) as total'))
            ->join('products', 'product_id', '=', 'products.id')
            ->where('invoice_id', $invoice->id)
            ->get();    

        $response = $this->get('/api/invoices/'. $invoice->id);
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $invoice->id,
                'number' => $invoice->number,
                'company' => [
                    'id' => $company->id,
                    'name' => $company->name,
                ],
                'status' => $invoice->status
            ]
        ]);
    }

    public function testShowInvoiceEndpointThrowsExceptionWhenInvalidField()
    {
        $response = $this->get('/api/invoices/123');
        $response->assertStatus(403);
        $this->assertEquals("Oops! Some required field was not filled.", $response['error']);
    }

    public function testShowInvoiceEndpointThrowsExceptionWhenInvoiceNotFound()
    {
        $invoiceMock = DB::table('invoices')->select('*')->where('status', 'draft')->first();
        $id = substr($invoiceMock->id, 0, -4);
        $invoiceMock->id = $id .'1111';

        $response = $this->post("/api/invoices/{$invoiceMock->id}/approve");
        $response->assertStatus(404);
        $this->assertEquals("Invoice not found", $response['error']);
    }

    public function testApproveInvoiceEndpoint()
    {
        $invoiceMock = DB::table('invoices')->select('*')->where('status', 'draft')->first();

        $response = $this->post("/api/invoices/{$invoiceMock->id}/approve");
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $invoiceMock->id,
                'number' => $invoiceMock->number,
                'status' => 'approved'
            ]
        ]);
    }

    public function testRejectInvoiceEndpoint()
    {
        $invoiceMock = DB::table('invoices')->select('*')->where('status', 'draft')->first();

        $response = $this->post("/api/invoices/{$invoiceMock->id}/reject");
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $invoiceMock->id,
                'number' => $invoiceMock->number,
                'status' => 'rejected'
            ]
        ]);
    }
}