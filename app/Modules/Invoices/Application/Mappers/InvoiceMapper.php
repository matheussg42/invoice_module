<?php

namespace App\Modules\Invoices\Application\Mappers;

use stdClass;
use Ramsey\Uuid\Uuid;
use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Dto\InvoiceDto;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Products\Domain\Services\ProductService;
use App\Modules\Companies\Domain\Services\CompanyService;

class InvoiceMapper
{
    public static function toEntity(stdClass $invoice): Invoice
    {
        return new Invoice(
            $invoice->id,
            Uuid::fromString($invoice->number),
            $invoice->date,
            $invoice->due_date,
            $invoice->company_id,
            StatusEnum::from($invoice->status),
        );
    }

    public static function toDTO(Invoice $invoice, string $type = "complete"): InvoiceDTO
    {
        
        $items = $type === "complete" ? ProductService::getInvoiceItems($invoice->getId()) : null ;
        return new InvoiceDTO(
            $invoice->getId(),
            $invoice->getNumber(),
            $invoice->getDate(),
            $invoice->getDueDate(),
            $type === "complete" ? CompanyService::getCompany($invoice->getCompanyId()) : null,
            $invoice->getStatus(),
            $items,
            $type === "complete" ? ProductService::getInvoiceItemsTotalPrice($items) : null,
            $type
        );
    }

}