<?php

namespace App\Modules\Invoices\Application\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class InvoiceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'uuid' => 'required|uuid',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge(['uuid' => $this->route('uuid')]);
    }

    public function withValidator($validator)
    {
        if ($validator->fails()) {
            throw new HttpResponseException(response()->json([
                'error'   => 'Oops! Some required field was not filled.',
                'status'=> false,
                'errors'=> $validator->errors(),
                'route' => $this->route()->uri
            ], 403));
       }
    }
}