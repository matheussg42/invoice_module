<?php

namespace App\Modules\Invoices\Domain\Repositories;

use App\Modules\Invoices\Domain\Entities\Invoice;
use Illuminate\Support\Facades\DB;
use stdClass;

class InvoiceRepository
{

    public function update(Invoice $invoice): void
    {
        DB::table('invoices')
        ->where('id', $invoice->getId())
        ->update([
            'status' => $invoice->getStatus(),
        ]);
    }

    public function getById(string $id): ?stdClass
    {
        return DB::table('invoices')->where('id', $id)->first();
    }
}