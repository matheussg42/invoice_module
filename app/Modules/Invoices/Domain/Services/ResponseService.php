<?php 

namespace App\Modules\Invoices\Domain\Services;

use Exception;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Application\Mappers\InvoiceMapper;
use Illuminate\Http\Response;

Class ResponseService{

    public static function success($config = array(), $id = null, Invoice $invoice, string $type = "complete")
    {
        $invoiceDTO = InvoiceMapper::toDTO($invoice, $type);
        $route = $config['route'];

        switch($config['type']){
            case 'approve':
                $response = [
                    'status'    => true,
                    'statusCode'=> 200,
                    'msg'       => "The Invoice {$id} was approved successfully!",
                    'data'      => $invoiceDTO->jsonSerialize(),
                    'url'       => route($route, $id)
                ];
                break;

            case 'reject':
                $response = [
                    'status'    => true,
                    'statusCode'=> 200,
                    'msg'       => "The Invoice {$id} was rejected successfully",
                    'data'      => $invoiceDTO->jsonSerialize(),
                    'url'       => route($route, $id)
                ];
                break;

            case 'show':
                $response = [
                    'status'    => true,
                    'statusCode'=> 200,
                    'msg'       => 'Request was made successfully!',
                    'data'      => $invoiceDTO->jsonSerialize(),
                    'url'       => route($route,$id)
                ];
                break;
        }

        return response()->json($response, Response::HTTP_OK);
    
    }

    public static function exception(string $route, string $id = null, Exception $e)
    {

        switch($e->getCode()){
            case 0:
                return response()->json([
                    'status'    => false,
                    'statusCode'=> 500,
                    'error'     => 'A issue occurred while processing the request.',
                    'url'       => $id != null ? route($route,$id) : route($route)
                ],500);
                break;

            default:
                return response()->json([
                    'status'    => false,
                    'statusCode'=> $e->getCode(),
                    'error'     => $e->getMessage() ?? 'A issue occurred while processing the request.',
                    'url'       => $id != null ? route($route,$id) : route($route)
                ], $e->getCode());
                break;
        }
    }
}