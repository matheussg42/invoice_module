<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Dto;

use Ramsey\Uuid\UuidInterface;
use App\Domain\Enums\StatusEnum;
use Illuminate\Support\Collection;
use App\Modules\Companies\Domain\Dto\CompanyDto;
use App\Modules\Companies\Domain\Entities\Company;
use App\Modules\Companies\Application\Mappers\CompanyMapper;

class InvoiceDto
{
    private $id;
    private $number;
    private $date;
    private $dueDate;
    private $company;
    private $status;
    private $items;
    private $total;
    private $type;

    public function __construct(
        string $id,
        UuidInterface $number,
        string $date,
        string $dueDate,
        Company $company = null,
        StatusEnum $status,
        Collection $items = null,
        int $total = null,
        string $type = 'complete'
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->date = $date;
        $this->dueDate = $dueDate;
        $this->company = $company;
        $this->status = $status;
        $this->items = $items;
        $this->total = $total;
        $this->type = $type;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNumber(): UuidInterface
    {
        return $this->number;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getDueDate(): string
    {
        return $this->dueDate;
    }

    public function getCompany(): CompanyDto
    {
        return CompanyMapper::toDTO($this->company);
    }

    public function getStatus(): StatusEnum
    {
        return $this->status;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function jsonSerialize()
    {
        $invoiceDto = [
            'id' => $this->getId(),
            'number' => $this->getNumber(),
            'date' => $this->getDate(),
            'due_date' => $this->getDueDate(),
            'status' => $this->getStatus()
        ];

        if($this->type === "complete") {
            $invoiceDto = array_merge($invoiceDto, [
                'company' => $this->getCompany()->jsonSerialize(),
                'items' => $this->getItems(),
                'total' => $this->getTotal()
            ]);
        }

        return $invoiceDto;
    }
}
