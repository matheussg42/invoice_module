<?php 

namespace App\Modules\Invoices\Domain\Entities;

use App\Domain\Enums\StatusEnum;
use Ramsey\Uuid\UuidInterface;

class Invoice
{
    private string $id;
    private UuidInterface $number;
    private string $date;
    private string $dueDate;
    private string $company_id;
    private StatusEnum $status;

    public function __construct(
        string $id,
        UuidInterface $number,
        string $date,
        string $dueDate,
        string $company_id,
        StatusEnum $status,
    ) {
        $this->id = $id;
        $this->number = $number;
        $this->date = $date;
        $this->dueDate = $dueDate;
        $this->company_id = $company_id;
        $this->status = $status;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNumber(): UuidInterface
    {
        return $this->number;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getDueDate(): string
    {
        return $this->dueDate;
    }

    public function getCompanyId(): string
    {
        return $this->company_id;
    }

    public function getStatus(): StatusEnum
    {
        return $this->status;
    }

    public function setStatus(StatusEnum $status): void
    {
        $this->status = $status;
    }

}