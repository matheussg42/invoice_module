<?php

namespace App\Modules\Invoices\UserInterface\Api\Controllers;

use LogicException;
use App\Infrastructure\Controller;
use App\Modules\Invoices\Domain\Services\InvoiceService;
use App\Modules\Invoices\Domain\Services\ResponseService;
use App\Modules\Invoices\Application\Requests\InvoiceRequest;
use App\Modules\Invoices\Domain\Exceptions\InvoiceNotFoundException;
use App\Modules\Companies\Domain\Exceptions\CompanyNotFoundException;
use App\Modules\Products\Domain\Exceptions\InvoiceItemsNotFoundException;

class InvoiceController extends Controller
{
    private $invoiceService;

    public function __construct(InvoiceService $invoiceService, )
    {
        $this->invoiceService = $invoiceService;
    }

    public function show(InvoiceRequest $request)
    {
        $data = $request->validated();

        try {
            $invoice = $this->invoiceService->getInvoice($data['uuid']);
            $responseInvoiceDto = ResponseService::success(array('type' => 'show','route' => 'invoices.show'), $data['uuid'], $invoice);
        } catch (InvoiceNotFoundException $e) {
            return ResponseService::exception('invoices.show', $data['uuid'], $e);
        } catch (CompanyNotFoundException $e) {
            return ResponseService::exception('invoices.show', $data['uuid'], $e);
        } catch (InvoiceItemsNotFoundException $e) {
            return ResponseService::exception('invoices.show', $data['uuid'], $e);
        }
        
        return $responseInvoiceDto;
    }

    public function approve(InvoiceRequest $request)
    {
        $data = $request->validated();
        try {
            $invoice = $this->invoiceService->approveInvoice($data['uuid']);
        } catch (LogicException $e) {
            return ResponseService::exception('invoices.approve', $data['uuid'], $e);
        } catch (InvoiceNotFoundException $e) {
            return ResponseService::exception('invoices.approve', $data['uuid'], $e);
        }

        return ResponseService::success(array('type' => 'approve', 'route' => 'invoices.approve'), $data['uuid'], $invoice, 'invoice');
    }

    public function reject(InvoiceRequest $request)
    {
        $data = $request->validated();
        try {
            $invoice = $this->invoiceService->rejectInvoice($data['uuid']);
        } catch (LogicException $e) {
            return ResponseService::exception('invoices.reject', $data['uuid'], $e);
        } catch (InvoiceNotFoundException $e) {
            return ResponseService::exception('invoices.reject', $data['uuid'], $e);
        }

        return ResponseService::success(array('type' => 'reject', 'route' => 'invoices.reject'), $data['uuid'], $invoice, 'invoice');
    }

}