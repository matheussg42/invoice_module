<?php

namespace App\Modules\Products\Domain\Exceptions;

use Exception;

class InvoiceItemsNotFoundException extends Exception
{
    public function __construct($message = "Invoice Items not found", $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}