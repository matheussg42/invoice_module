<?php

namespace App\Modules\Products\Domain\Repositories;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Collection;

class ProductRepository
{ 

    public function getInvoiceItems(string $invoiceId): Collection
    {
        $items = DB::table('invoice_product_lines')
        ->select(DB::raw('products.id, products.name, products.price, products.currency, quantity, (products.price * quantity) as total'))
        ->join('products', 'product_id', '=', 'products.id')
        ->where('invoice_id', $invoiceId)->get();

        return $items;
    }
}