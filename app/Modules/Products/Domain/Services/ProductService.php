<?php

namespace App\Modules\Products\Domain\Services;

use Illuminate\Support\Collection;
use App\Modules\Products\Domain\Repositories\ProductRepository;
use App\Modules\Products\Domain\Exceptions\InvoiceItemsNotFoundException;

class ProductService
{

    public static function getInvoiceItems(string $invoiceId): Collection
    {
        $productRepository = new ProductRepository();
        $invoiceItems = $productRepository->getInvoiceItems($invoiceId);

        if (!$invoiceItems->jsonSerialize()) {
            throw new InvoiceItemsNotFoundException();
        }  

        return $invoiceItems;
    }

    public static function getInvoiceItemsTotalPrice(Collection $items): int
    {
        $totalPrice = 0;

        foreach ($items as $item) {
            $totalPrice += $item->total;
        }

        return $totalPrice;
    }
}