<?php

namespace App\Modules\Companies\Domain\Repositories;

use Illuminate\Support\Facades\DB;
use stdClass;

class CompanyRepository
{ 

    public function getById(string $id): ?stdClass
    {
        return DB::table('companies')->where('id', $id)->first();
    }
}