<?php

namespace App\Modules\Companies\Domain\Exceptions;

use Exception;

class CompanyNotFoundException extends Exception
{
    public function __construct($message = "Company not found", $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}