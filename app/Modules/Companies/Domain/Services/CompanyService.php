<?php

namespace App\Modules\Companies\Domain\Services;

use App\Modules\Companies\Domain\Entities\Company;
use App\Modules\Companies\Domain\Repositories\CompanyRepository;
use App\Modules\Companies\Domain\Exceptions\CompanyNotFoundException;

class CompanyService
{

    public static function getCompany(string $id): Company
    {
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->getById($id);

        if (!$company) {
            throw new CompanyNotFoundException();
        }  

        return new Company(
            $company->id,
            $company->name,
            $company->street,
            $company->city,
            $company->zip,
            $company->phone,
            $company->email
        );
    }
}