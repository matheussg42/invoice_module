<?php

namespace App\Modules\Companies\Application\Mappers;

use App\Modules\Companies\Domain\Entities\Company;
use App\Modules\Companies\Domain\Dto\CompanyDto;

class CompanyMapper
{
    public static function toEntity(CompanyDto $dto): Company
    {
        return new Company(
            $dto->getId(),
            $dto->getName(),
            $dto->getStreet(),
            $dto->getCity(),
            $dto->getZip(),
            $dto->getPhone(),
            $dto->getEmail()
        );
    }

    public static function toDTO(Company $invoice): CompanyDto
    {
        
        return new CompanyDto(
            $invoice->getId(),
            $invoice->getName(),
            $invoice->getStreet(),
            $invoice->getCity(),
            $invoice->getZip(),
            $invoice->getPhone(),
            $invoice->getEmail()
        );
    }
}